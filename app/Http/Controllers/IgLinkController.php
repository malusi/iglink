<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class IgLinkController extends Controller
{
    public function show() {
        $url = 'https://fs.favourup.com/api/v1/shoppable_gallery/6mO2ulYz2Xm';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url)->getBody();
        $data = json_decode($response, true);
        
        return view('gallery', compact('data'));
    }
}
