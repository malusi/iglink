@extends('main')

@section('title', 'Instagram Gallery')

@section('content')

<div class="container">
    
    <link-gallery :data='{{json_encode($data)}}'></link-gallery>

</div>

@endsection
