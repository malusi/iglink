<nav class="navbar fixed-top navbar-expand-lg navbar-light topNav">
    <a href="https://favourup.com/" target="_blank"><img class="navbar-brand" src="/img/logo-abbr-40.png" alt="logo" width="50" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarNav" class="collapse navbar-collapse">
      <div class="navbar-nav">
        <li><a class="nav-item nav-link" href="" target="_blank">Favourup</a></li>
        <li><a class="nav-item nav-link" href="#">Shop the look</a></li>
      </div>
    </div>
  </nav>