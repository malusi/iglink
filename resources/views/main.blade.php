<!DOCTYPE html>
<html lang="en">
	<head>
		@include('partials._head')
		@yield('stylesheets')
	</head>

	<body>
		@include('partials._topbanner')
		<div id="app">

			
			@yield('content')

		</div>
		
		@include('partials._javascript')
		@yield('scripts')
		@stack('scripts')
	</body>
</html>
